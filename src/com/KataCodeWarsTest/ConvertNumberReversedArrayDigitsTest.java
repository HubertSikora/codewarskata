package com.KataCodeWarsTest;

import com.KataCodeWars.ConvertNumberReversedArrayDigits;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class ConvertNumberReversedArrayDigitsTest {
    @Test
    public void tests() {
        assertArrayEquals(new int[]{1, 3, 2, 5, 3}, ConvertNumberReversedArrayDigits.digitize(35231));
    }
}
